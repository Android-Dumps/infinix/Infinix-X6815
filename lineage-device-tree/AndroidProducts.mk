#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Infinix-X6815.mk

COMMON_LUNCH_CHOICES := \
    lineage_Infinix-X6815-user \
    lineage_Infinix-X6815-userdebug \
    lineage_Infinix-X6815-eng
